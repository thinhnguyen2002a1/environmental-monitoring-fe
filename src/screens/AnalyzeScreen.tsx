import React, { useEffect, useState } from "react";
import { View, ScrollView, Switch, Text, StyleSheet } from "react-native";
import LineChartScreen from "./LineChartScreen";
import axios from "axios";
import LoadProgress from "../components/LoadProgress";
import { Dropdown } from "react-native-element-dropdown";
import ReloadButton from "../components/ReloadButton";
const dataInitial = [
  {
    label: "NodeID 1",
    value: 1, // Thay vì '1'
  },
  {
    label: "NodeID 2",
    value: 2, // Thay vì '2'
  },
  // {
  //   label: "NodeID 3",
  //   value: "3", // Thay vì '2'
  // },
  // {
  //   label: "NodeID 4",
  //   value: "4", // Thay vì '2'
  // },

  // Add more items if need ed
];
export default function AnalyzeScreen() {
  const [dataChart, setDataChart] = useState([]);

  const [selectedNode, setSelectedNode] = useState(1); // Mặc định là NodeID 1
  const [value, setValue] = useState(1);
  const [loadProgress, setLoadProgress] = useState(false);
  useEffect(() => {
    try {
      fetchData();

      // Set up interval to fetch data every 30 seconds
      const intervalId = setInterval(() => {
        fetchData();
      }, 60000);

      // Cleanup the interval on component unmount
      return () => clearInterval(intervalId);
    } catch (error) {
      console.log(error);
    }
  }, [selectedNode]);
  const renderItem = (item) => {
    console.log("value of item.label " + item.label);
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
      </View>
    );
  };
  const handleReload = () => {
    // Xử lý khi nút "reload" được nhấn
    console.log("Reloading...");
    fetchData();
  };
  const fetchData = async () => {
    try {
      console.log("value of select node" + selectedNode);
      setLoadProgress(true);
      const response = await axios.get(
        "https://environment-monitoring-73aq.onrender.com/sensor"
      );
      if (selectedNode == 1) {
        const node1Data = response.data.filter((sensor) => sensor.nodeID === 1);
        console.log("value of node1 data" + JSON.stringify(node1Data));

        await setDataChart(node1Data);
        setLoadProgress(false);
      } else {
        const node2Data = response.data.filter((sensor) => sensor.nodeID === 2);
        console.log("value of node2 data" + JSON.stringify(node2Data));
        await setDataChart(node2Data); // Gán lại giá trị cho dataChart khi chuyển từ NodeID 1 sang NodeID 2

        setLoadProgress(false);
      }
    } catch (error) {
      console.error("Error in fetchData:", error);
      // Hoặc log stack trace của lỗi nếu bạn muốn
      console.error(error.stack);
    }
  };

  if (loadProgress) {
    return <LoadProgress divide={1} />;
  } else {
    return (
      <ScrollView>
        {/* Toggle Button for Light Chart */}

        <View style={{ flexDirection: "column", flex: 1 }}>
          <View style={{ flexDirection: "row" }}>
            <Dropdown
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              style={styles.dropdown}
              placeholder={
                selectedNode == 1 ? dataInitial[0].label : dataInitial[1].label
              }
              data={dataInitial}
              value={value}
              maxHeight={300}
              // onChangeText={() => "label"}
              labelField={"label"}
              valueField={"value"}
              onChange={(item) => {
                console.log("label index 1 is ", dataInitial[1].label);
                setSelectedNode(item.value);
                setValue(item.value);
              }}
              renderItem={renderItem}
            />
            <View
              style={{
                alignItems: "center",
                marginTop: 25,
                marginLeft: 16,
                marginRight: 12,
              }}
            >
              <ReloadButton onPress={handleReload} />
            </View>
          </View>

          <View>
            <LineChartScreen
              key={`TemperatureChart-${selectedNode}`}
              data={dataChart}
              title="Temperature Chart"
              color="255, 99, 71"
            />
            <LineChartScreen
              key={`HumidityChart-${selectedNode}`}
              data={dataChart}
              title="Humidity Chart"
              color="0, 128, 0"
            />

            <LineChartScreen
              key={`LightChart-${selectedNode}`}
              data={dataChart}
              title="Light Chart"
              color="0, 0, 128"
            />
            <LineChartScreen
              key={`SoilMoistureChart-${selectedNode}`}
              data={dataChart}
              title="Soil Moisture Chart"
              color="128, 0, 0"
            />
          </View>

          <View style={{ height: 100, flex: 1 }}></View>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  dropdown: {
    width: "80%",
    marginTop: 25,
    marginLeft: 18,
    height: 50,
    backgroundColor: "white",
    borderRadius: 12,
    padding: 12,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  icon: {
    marginRight: 5,
  },
  item: {
    padding: 17,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  placeholderStyle: {
    fontSize: 16,
  },
  selectedTextStyle: {
    fontSize: 16,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
  },
});
