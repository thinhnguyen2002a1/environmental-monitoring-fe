import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import { LineChart } from "react-native-chart-kit";

const LineChartScreen = ({ data, title, color }) => {
  const [previousData, setPreviousData] = useState([]);

  useEffect(() => {
    // Lưu trữ dữ liệu hiện tại để so sánh ở lần fetch tiếp theo
    if (newData.length > 0) {
      setPreviousData(data);
    }
  }, [data]);

  // Lọc dữ liệu chỉ từ sensor "dht20" và những dữ liệu có giá trị mới so với dữ liệu trước
  const dht20Data =
    data.find((entry) => entry.sensorId === "dht20")?.value || [];
  const newData = dht20Data.filter((entry) => {
    const timestamp = new Date(entry.timestamp).getTime();
    const prevEntry = previousData.find((prev) => prev.sensorId === "dht20");
    if (!prevEntry) {
      return true; // Nếu không có dữ liệu trước đó, lấy tất cả
    }

    const prevTimestamp = new Date(prevEntry.timestamp).getTime();
    return (
      entry.temperature !== prevEntry.temperature ||
      entry.humidity !== prevEntry.humidity ||
      timestamp !== prevTimestamp
    );
  });

  // Lọc dữ liệu chỉ từ sensor "light" và những dữ liệu có giá trị mới so với dữ liệu trước
  const lightData =
    data.find((entry) => entry.sensorId === "light")?.value || [];

  // console.log("value of lightData" + JSON.stringify(lightData));

  const newLightData = lightData.filter((entry) => {
    const timestamp = new Date(entry.timestamp).getTime();
    const prevEntry = previousData.find((prev) => prev.sensorId === "light");
    if (!prevEntry) {
      return true; // Nếu không có dữ liệu trước đó, lấy tất cả
    }

    const prevTimestamp = new Date(prevEntry.timestamp).getTime();
    return timestamp !== prevTimestamp || entry.light !== prevEntry.light;
  });

  // Loại bỏ giá trị null khỏi mảng dữ liệu mới
  const newDataWithoutNull = newData.filter((entry) =>
    title === "Temperature Chart"
      ? entry.temperature !== null
      : entry.humidity !== null
  );

  // Loại bỏ giá trị null khỏi mảng dữ liệu mới của biểu đồ "Light Chart"
  const newLightDataWithoutNull = newLightData.filter(
    (entry) => entry.light !== null
  );

  const getFormattedTime = (timestamp) => {
    const date = new Date(timestamp);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();
    return ``;
  };

  const getLabels = () => {
    const labels = newDataWithoutNull.map((entry) =>
      getFormattedTime(entry.timestamp)
    );
    // Loại bỏ những nhãn không có dữ liệu
    const filteredLabels = labels.filter((label, index) =>
      title === "Temperature Chart"
        ? newDataWithoutNull[index].temperature !== null
        : newDataWithoutNull[index].humidity !== null
    );

    // console.log("labels", filteredLabels);
    return labels;
  };

  // Hàm lấy nhãn cho biểu đồ "Light Chart"
  const getLightLabels = () => {
    const lightLabels = newLightDataWithoutNull.map((entry) =>
      getFormattedTime(entry.timestamp)
    );
    // Loại bỏ những nhãn không có dữ liệu
    const filteredLightLabels = lightLabels.filter(
      (label, index) => newLightDataWithoutNull[index].light !== null
    );

    // console.log("light labels", filteredLightLabels);
    return lightLabels;
  };

  const getDataset = () => {
    const temperatureData = newDataWithoutNull.map((entry, index) =>
      title === "Temperature Chart" ? entry.temperature : null
    );
    const humidityData = newDataWithoutNull.map((entry) =>
      title === "Humidity Chart" ? entry.humidity : null
    );

    // Kiểm tra giá trị đầu tiên và đặt thành undefined nếu là 0
    if (temperatureData.length > 0 && temperatureData[0] === 0) {
      temperatureData[0] = undefined;
    }

    return {
      datasets: [
        {
          data: temperatureData,
          color: (opacity = 1) => `rgba(${color}, ${opacity})`,
          strokeWidth: 2,
        },
        {
          data: humidityData,
          color: (opacity = 1) => `rgba(${color}, ${opacity})`,
          strokeWidth: 2,
        },
      ],
    };
  };

  // Hàm lấy dữ liệu cho biểu đồ "Light Chart"
  const getLightDataset = () => {
    const lightChartData = newLightDataWithoutNull.map((entry, index) =>
      title === "Light Chart" ? entry.light : null
    );

    // Kiểm tra giá trị đầu tiên và đặt thành undefined nếu là 0
    if (lightChartData.length > 0 && lightChartData[0] === 0) {
      lightChartData[0] = undefined;
    }

    return {
      datasets: [
        {
          data: lightChartData,
          color: (opacity = 1) => `rgba(${color}, ${opacity})`,
          strokeWidth: 2,
        },
      ],
    };
  };

  const soilMoistureData =
    data.find((entry) => entry.sensorId === "soil_moisture")?.value || [];
  const newSoilMoistureData = soilMoistureData.filter((entry) => {
    const timestamp = new Date(entry.timestamp).getTime();
    const prevEntry = previousData.find(
      (prev) => prev.sensorId === "soil_moisture"
    );

    if (!prevEntry) {
      return true;
    }

    const prevTimestamp = new Date(prevEntry.timestamp).getTime();
    return (
      entry.soil_moisture !== prevEntry.soil_moisture || // Thay đổi tên field nếu cần thiết
      timestamp !== prevTimestamp
    );
  });

  const newSoilMoistureDataWithoutNull = newSoilMoistureData.filter(
    (entry) => entry.soil_moisture !== null // Thay đổi tên field nếu cần thiết
  );

  // Hàm lấy nhãn cho biểu đồ "Soil Moisture Chart"
  const getSoilMoistureLabels = () => {
    const soilMoistureLabels = newSoilMoistureDataWithoutNull.map((entry) =>
      getFormattedTime(entry.timestamp)
    );

    // Loại bỏ những nhãn không có dữ liệu
    const filteredSoilMoistureLabels = soilMoistureLabels.filter(
      (label, index) =>
        newSoilMoistureDataWithoutNull[index].soil_moisture !== null
    );

    // console.log("soil moisture labels", filteredSoilMoistureLabels);
    return soilMoistureLabels;
  };

  // Hàm lấy dữ liệu cho biểu đồ "Soil Moisture Chart"
  const getSoilMoistureDataset = () => {
    const soilMoistureChartData = newSoilMoistureDataWithoutNull.map(
      (entry, index) =>
        title === "Soil Moisture Chart" ? entry.soil_moisture : null // Thay đổi tên field nếu cần thiết
    );

    // Kiểm tra giá trị đầu tiên và đặt thành undefined nếu là 0
    if (soilMoistureChartData.length > 0 && soilMoistureChartData[0] === 0) {
      soilMoistureChartData[0] = undefined;
    }

    return {
      datasets: [
        {
          data: soilMoistureChartData,
          color: (opacity = 1) => `rgba(${color}, ${opacity})`,
          strokeWidth: 2,
        },
      ],
    };
  };

  return (
    <View style={styles.container}>
      <Text style={styles.chartTitle}>{title}</Text>
      {title === "Light Chart" && newLightDataWithoutNull.length > 0 && (
        <LineChart
          data={{
            labels: getLightLabels(),
            ...getLightDataset(),
          }}
          width={350}
          height={220}
          yAxisSuffix=""
          chartConfig={{
            backgroundGradientFrom: "#fff",
            backgroundGradientTo: "#fff",
            decimalPlaces: 2,
            color: (opacity = 1) => `rgba(${color}, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            style: {
              borderRadius: 16,
            },
          }}
          bezier
          style={styles.chartStyle}
        />
      )}
      {title !== "Light Chart" &&
        title != "Soil Moisture Chart" &&
        newDataWithoutNull.length > 0 && (
          <LineChart
            data={{
              labels: getLabels(),
              ...getDataset(),
            }}
            width={350}
            height={220}
            yAxisSuffix={title === "Temperature Chart" ? "°C" : "%"}
            chartConfig={{
              backgroundGradientFrom: "#fff",
              backgroundGradientTo: "#fff",
              decimalPlaces: 2,
              color: (opacity = 1) => `rgba(${color}, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
              style: {
                borderRadius: 16,
              },
            }}
            bezier
            style={styles.chartStyle}
          />
        )}
      {title === "Soil Moisture Chart" &&
        newSoilMoistureDataWithoutNull.length > 0 && (
          <LineChart
            data={{
              labels: getSoilMoistureLabels(),
              ...getSoilMoistureDataset(),
            }}
            width={350}
            height={220}
            yAxisSuffix="%"
            chartConfig={{
              backgroundGradientFrom: "#fff",
              backgroundGradientTo: "#fff",
              decimalPlaces: 2,
              color: (opacity = 1) => `rgba(${color}, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
              style: {
                borderRadius: 16,
              },
            }}
            bezier
            style={styles.chartStyle}
          />
        )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    margin: 12,
  },
  chartTitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
  },
  chartStyle: {
    marginVertical: 8,
    borderRadius: 16,
  },
});

export default LineChartScreen;
