import React, { useEffect, useState } from "react";
import { StatusBar } from "expo-status-bar";
import {
  View,
  Image,
  Text,
  FlatList,
  ScrollView,
  StyleSheet,
} from "react-native";
import LoadProgress from "../components/LoadProgress";

import { stylesGlobal } from "../components/Style/style";
import CardGrid from "../components/CardGrid";
import {
  useFonts,
  Exo2_400Regular,
  Exo2_700Bold,
} from "@expo-google-fonts/exo-2";
import axios from "axios"; // Import Axios
import ReloadButton from "../components/ReloadButton";
import { Dropdown } from "react-native-element-dropdown";
const dataInitial = [
  {
    label: "NodeID 1",
    value: "1", // Thay vì '1'
  },
  {
    label: "NodeID 2",
    value: "2", // Thay vì '2'
  },
  // {
  //   label: "NodeID 3",
  //   value: "3", // Thay vì '2'
  // },
  // {
  //   label: "NodeID 4",
  //   value: "4", // Thay vì '2'
  // },

  // Add more items if need ed
];
const renderItem = (item) => {
  console.log("value of item.label " + item.label);
  return (
    <View style={styles.item}>
      <Text style={styles.textItem}>{item.label}</Text>
    </View>
  );
};
export default function HomeScreen() {
  const [dataValue, setDataValue] = useState([
    {
      id: "1",
      type: "Temperature",
      value: "0°C",
      source: require("../../assets/temp.png"),
      timestamp: "",
    },
    {
      id: "2",
      type: "Humidity",
      value: "0%",
      source: require("../../assets/humid.png"),
      timestamp: "",
    },
    {
      id: "3",
      type: "Light",
      value: "Good",
      source: require("../../assets/light.png"),
      timestamp: "",
    },
    {
      id: "4",
      type: "Soil Moisture",
      value: "Low",
      source: require("../../assets/soil_mosture.png"),
      timestamp: "",
    },
  ]);
  const [timeCurrent, setTimeCurrent] = useState(null);
  const [selectedNode, setSelectedNode] = useState(1); // Mặc định là NodeID 1
  const [value, setValue] = useState(1);
  const [loadProgress, setLoadProgress] = useState(false);

  useEffect(() => {
    fetchData();
    // Set up interval to fetch data every 30 seconds
    const intervalId = setInterval(() => {
      fetchData();
    }, 60000);

    // Cleanup the interval on component unmount
    return () => clearInterval(intervalId);
  }, [selectedNode]);

  const fetchData = async () => {
    try {
      setLoadProgress(true);

      const response = await axios({
        method: "get",
        url: "https://environment-monitoring-73aq.onrender.com/sensor",
        headers: {
          "Content-Type": "application/json",
        },
      });

      // console.log("value of response", response);

      // Create an object to store latest data for each sensor
      const latestData = {};
      const sensorData = {};
      let processValue = 0;
      response.data.forEach((sensor) => {
        if (!sensorData[selectedNode]) {
          sensorData[selectedNode] = {};
        }
        if (!latestData[selectedNode]) {
          latestData[selectedNode] = {};
        }
        const sortedSensorData = sensor.value.sort((a, b) => {
          const timestampA = new Date(a.timestamp).getTime();
          const timestampB = new Date(b.timestamp).getTime();
          return timestampB - timestampA;
        });

        const latestSensorData = sortedSensorData[0];

        // Extracting values based on sensor type
        let temperature, humidity, light, soilMoisture;
        // console.log(
        //   "value of latestSensorData " + JSON.stringify(latestSensorData)
        // );
        if (sensor.sensorId === "dht20") {
          temperature = latestSensorData.temperature;
          humidity = latestSensorData.humidity;
        } else if (sensor.sensorId === "light") {
          light =
            latestSensorData.light !== null ? latestSensorData.light : null;
        } else if (sensor.sensorId === "soil_moisture") {
          soilMoisture =
            latestSensorData.soil_moisture !== undefined
              ? latestSensorData.soil_moisture
              : null;
        } else {
          console.log("difference case");
        }
        console.log("nodeID ", selectedNode);
        // console.log("light value  ", latestSensorData.light);

        // Update the latestData object
        if (selectedNode == 1) {
          if (processValue >= 3) return;

          latestData[selectedNode][sensor.sensorId] = {
            temperature,
            humidity,
            timestamp: latestSensorData.timestamp,
            light,
            soilMoisture,
          };
          processValue++;
        } else {
          processValue++;
          if (processValue >= 3) {
            latestData[selectedNode][sensor.sensorId] = {
              temperature,
              humidity,
              timestamp: latestSensorData.timestamp,
              light,
              soilMoisture,
            };
          }
        }

        console.log("latestSensorData: ", latestSensorData);
      });

      // Update the state with the latest data for each sensor
      setDataValue((prevData) => [
        {
          ...prevData[0],
          value: `${latestData[selectedNode]["dht20"].temperature}°C`,
          timestamp: `${formattedTimestamp(
            latestData[selectedNode]["dht20"].timestamp
          )}`,
        },
        {
          ...prevData[1],
          value: `${latestData[selectedNode]["dht20"].humidity}%`,
          timestamp: `${formattedTimestamp(
            latestData[selectedNode]["dht20"].timestamp
          )}`,
        },
        {
          ...prevData[2],
          value:
            latestData[selectedNode]["light"] !== null
              ? `${latestData[selectedNode]["light"].light}`
              : "N/A",
          timestamp: `${formattedTimestamp(
            latestData[selectedNode]["light"].timestamp
          )}`,
        },
        {
          ...prevData[3],
          value:
            latestData[selectedNode]["soil_moisture"] !== null
              ? `${latestData[selectedNode]["soil_moisture"].soilMoisture}`
              : "N/A",
          timestamp: `${formattedTimestamp(
            latestData[selectedNode]["soil_moisture"].timestamp
          )}`,
        },
      ]);
      setLoadProgress(false);
      setTimeCurrent(formattedTimestamp(new Date()));

      console.log("Latest Data:", latestData);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const formattedTimestamp = (timestamp) => {
    const date = new Date(timestamp);
    const formattedDate = date.toLocaleString("vi-VN", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
    return formattedDate;
  };
  const [switchStates, setSwitchStates] = useState({
    1: true,
    2: true,
    3: true,
    4: true,
  });

  const toggleSwitch = (itemId, value) => {
    setSwitchStates((prevState) => ({
      ...prevState,
      [itemId]: value,
    }));
  };
  const handleReload = () => {
    // Xử lý khi nút "reload" được nhấn
    console.log("Reloading...");
    fetchData();
  };

  let [fontsLoaded] = useFonts({
    Exo2_400Regular,
    Exo2_700Bold,
  });

  if (!fontsLoaded) {
    return null;
  }
  if (loadProgress) {
    return <LoadProgress divide={1} />;
  } else {
    return (
      <>
        <View
          style={[
            stylesGlobal.container,
            {
              marginTop: 20,
              marginLeft: 16,
              flexDirection: "row",
              backgroundColor: null,
            },
          ]}
        >
          <Image
            source={require("../../assets/environmental-monitoring-logo.png")}
            style={{
              width: 60,
              height: 60,
              flex: 1,
              borderRadius: 10,
            }}
          />
          <View style={{ flexDirection: "column", flex: 4 }}>
            <View>
              <Text style={stylesGlobal.header}>Hi Noal</Text>
            </View>
            <View>
              <Text style={stylesGlobal.header2}>Have a good day!</Text>
            </View>
          </View>
          <View style={{ flex: 3 }}>
            <Text
              style={[
                stylesGlobal.header2,
                { alignContent: "center", color: "green" },
              ]}
            >
              {timeCurrent}
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <ReloadButton onPress={handleReload} />
          </View>
        </View>
        <View style={{ flexDirection: "column" }}>
          {/* <Text style={stylesGlobal.header}>Select NodeID:</Text> */}
          <Dropdown
            placeholderStyle={styles.placeholderStyle}
            selectedTextStyle={styles.selectedTextStyle}
            style={styles.dropdown}
            placeholder={
              selectedNode == 1
                ? dataInitial[0].label
                : dataInitial[selectedNode - 1].label
            }
            data={dataInitial}
            value={value}
            maxHeight={300}
            // onChangeText={() => "label"}
            labelField={"label"}
            valueField={"value "}
            onChange={(item) => {
              console.log("label index 0 is ", dataInitial[0].label);
              setSelectedNode(+item.value);
              setValue(+item.value);
            }}
            renderItem={renderItem}
          />
        </View>
        {/* Grid area with FlatList */}
        <FlatList
          data={dataValue}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <CardGrid
              item={item}
              switchStates={switchStates}
              toggleSwitch={toggleSwitch}
            />
          )}
          numColumns={2} // Set the number of columns to 2
          contentContainerStyle={{ padding: 16 }}
        />
        {/* <StatusBar style="auto" /> */}
      </>
    );
  }
}

const styles = StyleSheet.create({
  dropdown: {
    width: "94%",
    margin: 16,
    height: 50,
    backgroundColor: "white",
    borderRadius: 12,
    padding: 12,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  icon: {
    marginRight: 5,
  },
  item: {
    padding: 17,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  placeholderStyle: {
    fontSize: 16,
  },
  selectedTextStyle: {
    fontSize: 16,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
  },
});
